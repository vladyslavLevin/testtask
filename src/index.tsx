import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { form } from './reducers/form';
import { IStore } from './types/index';
import App from './App';
import './index.css';
import { changeActions } from './actions/form';
import registerServiceWorker from './registerServiceWorker';
import { Router, Route } from 'react-router';
import { createBrowserHistory } from 'history';

const store = createStore<IStore, changeActions, any, any>(form, {
  inputValue1: '',
  inputValue2: '',
  selectValue:  '',
  selectValueMultiple: [],
  isFormDirty: false,
  queryString:'',
});
export const history = createBrowserHistory();

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Route path='/' component={App} />
    </Router>
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
(window as any).__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;
