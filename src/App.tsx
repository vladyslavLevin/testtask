import * as React from 'react';
import './App.css';
import FormContainer from './containers/FormContainer';

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <FormContainer />
      </div>
    );
  }
}

export default App;
