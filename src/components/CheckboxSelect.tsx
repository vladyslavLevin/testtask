import * as React from 'react';
import {
  Checkbox,
  FormControl,
  InputLabel,
  Input,
  ListItemText,
  MenuItem,
  Select,
} from '@material-ui/core';

interface IProps {
  multiple?: boolean;
  changeCheckbox: (fieldName: string, value: string) => void;
  selectValue: any,
}

interface IEvent {
  target: {
    value: any;
  };
}

const itemsSet = [
  'Item 1',
  'Item 2',
  'Item 3',
  'Item 4',
  'Item 5',
  'Item 6',
];

class CheckboxSelect extends React.Component<IProps> {

  public handleChange = (event: IEvent) => {
    let fieldName: string = 'selectValue';
    if (this.props.multiple) {
      fieldName = 'selectValueMultiple'
    };
    this.props.changeCheckbox(fieldName, event.target.value)
  };

  public renderValue = (selected: string[]): any => {
    if (this.props.multiple) { 
      return selected.join(', ');
    }
    return selected;
  };

  public render() {
    return (
        <FormControl fullWidth>
          <InputLabel htmlFor="select-multiple-checkbox">Checkbox select</InputLabel>
          <Select
            multiple={this.props.multiple}
            value={this.props.selectValue}
            onChange={this.handleChange}
            input={<Input id="select-multiple-checkbox" />}
            renderValue={this.renderValue}
          >
            {itemsSet.map((name: string) => (
              <MenuItem key={name} value={name}>
                <Checkbox checked={this.props.selectValue.indexOf(name) > -1} />
                <ListItemText primary={name} />
              </MenuItem>
            ))}
          </Select>
        </FormControl>
    );
  }
}

export default CheckboxSelect;