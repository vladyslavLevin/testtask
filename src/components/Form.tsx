import * as React from 'react';
import CheckboxSelect from './CheckboxSelect'
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { history } from '../index';

interface IProps {
  inputValue1: string;
  inputValue2: string;
  selectValue:  string;
  selectValueMultiple: string[];
  isFormDirty: boolean;
  СhangeInputValue: (fieldName: string, value: string) => void;
  ClearForm: () => void;
  queryString: string;
}

interface IState {
  count: number;
}

interface IEvent {
  target: {
    name: string,
    value: string,
  };
}

class Form extends React.Component<IProps, IState> {

  public state: IState = {
    count: 0,
  };

  public handleChangeTextField = (event: IEvent) => {
    this.props.СhangeInputValue(event.target.name, event.target.value);
  }

  public render() { 
    history.push(this.props.queryString)

    return (
      <form>
        <Grid container spacing={16}>
          <Grid item xs={6}>
            <TextField
              fullWidth
              value={this.props.inputValue1}
              name="inputValue1"
              onChange={this.handleChangeTextField}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              fullWidth
              value={this.props.inputValue2}
              name="inputValue2"
              onChange={this.handleChangeTextField}
            />
          </Grid>
          <Grid item xs={6}>
            <CheckboxSelect
              multiple
              changeCheckbox={this.props.СhangeInputValue}
              selectValue={this.props.selectValueMultiple}
            />
          </Grid>
          <Grid item xs={6}>
            <CheckboxSelect
              changeCheckbox={this.props.СhangeInputValue}
              selectValue={this.props.selectValue}
            />
          </Grid>
          <Grid item container xs={12} justify="center">
            {this.props.isFormDirty && (
              <Button
                variant="outlined"
                onClick={this.props.ClearForm}
              >
                Clear
              </Button>
            )}
          </Grid>
        </Grid>
      </form>
    );
  }
}

export default Form;