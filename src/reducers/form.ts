import { changeActions, CHANGE_INPUT_VALUE, CLEAR_FORM } from '../actions/form';
import { IStore } from '../types/index';

export function form(state: IStore, action: changeActions): IStore {
  switch (action.type) {
    case CHANGE_INPUT_VALUE:
    let resultQueryString = `?${action.payload.fieldName}=${action.payload.value}`;
    if (state.queryString) {
      const transformedToObjectQueryString = state.queryString.substr(1).split('&').map((item) => {
        const fieldName = item.split('=')[0];
        const value = item.split('=')[1]; 
        return { fieldName, value };
      });
      const isValueExistsInQueryString = !!transformedToObjectQueryString.find((item) => item.fieldName === action.payload.fieldName);
      let newQueryParams = [...transformedToObjectQueryString, { fieldName: action.payload.fieldName, value: action.payload.value }];
      if (isValueExistsInQueryString) {
        newQueryParams = transformedToObjectQueryString.map((item) => {
          if (item.fieldName === action.payload.fieldName) {
            return {...item, value: action.payload.value }
          }
        return item;
        });
      };
      const updatedStringOfParams = newQueryParams.map((item) => `${item.fieldName}=${item.value}`).join('&');
      resultQueryString = `?${updatedStringOfParams}`;
    }
      return {
          ...state,
          [action.payload.fieldName]: action.payload.value,
          isFormDirty: true,
          queryString: resultQueryString,
      };
    case CLEAR_FORM:
      return {
        inputValue1: '',
        inputValue2: '',
        selectValue:  '',
        selectValueMultiple: [],
        isFormDirty: false,
        queryString: '',
      };
  }
  return state;
}
