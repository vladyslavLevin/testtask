import Form from '../components/Form';
import * as actions from '../actions/form';
import { IStore } from '../types/index';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

export function mapStateToProps({ 
  inputValue1,
  inputValue2,
  selectValue,
  selectValueMultiple,
  isFormDirty,
  queryString,
 }: IStore) {
  return {
    selectValueMultiple,
    inputValue1,
    inputValue2,
    isFormDirty,
    selectValue,
    queryString,
  }
}

export function mapDispatchToProps(dispatch: Dispatch<actions.changeActions>) {
  return {
    СhangeInputValue: (fieldName: string, value: string) => dispatch(actions.СhangeInputValue(fieldName, value)),
    ClearForm: () => dispatch(actions.ClearForm()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Form);