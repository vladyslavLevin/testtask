export interface IStore {
  inputValue1: string;
  inputValue2: string;
  selectValue:  string;
  selectValueMultiple: string[];
  isFormDirty: boolean;
  queryString: string;
  }
