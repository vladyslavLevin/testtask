export const CHANGE_INPUT_VALUE = 'CHANGE_INPUT_VALUE';
export type CHANGE_INPUT_VALUE = typeof CHANGE_INPUT_VALUE;

export const CLEAR_FORM = 'CLEAR_FORM';
export type CLEAR_FORM = typeof CLEAR_FORM;

export interface IСhangeInputValue {
  type: CHANGE_INPUT_VALUE;
  payload: {
    fieldName: string,
    value: string,
  }
}

export interface IClearForm {
  type: CLEAR_FORM;
}

export type changeActions = IСhangeInputValue | IClearForm;

export const СhangeInputValue = (fieldName: string, value: string): IСhangeInputValue => {
  return {
      type: CHANGE_INPUT_VALUE,
      payload: {
        fieldName,
        value,
      }
  }
}

export const ClearForm = (): IClearForm => {
  return {
      type: CLEAR_FORM,
  }
}
